.. pwiki documentation master file, created by
   sphinx-quickstart on Tue Jan 18 16:14:36 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to pwiki's documentation!
=================================

**pwiki** is a Python library which makes interacting with the MediaWiki API simple and easy.  It can be used with sites such as Wikipedia, or virtually any other website that runs on MediaWiki.


Installation
------------

Install with pip:

.. code-block:: bash

   pip install pwiki


.. toctree::
   :maxdepth: 2
   :caption: Contents:
   
   examples
   reference



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
